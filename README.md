##Front-end recruitment test - Seatbite

Thank you for taking our Front-end recruitment test. The challenge is divided into two parts:
1.	Your task is to develop an application that allows you to place orders for food and drinks in a football stadium.
2.	Answer some questions.


________________________________________

We recommend that you do not spend more than 5 days on the test as long as the following items are served:

1.	Your application works as described in the task by getting the data from the JSON file. 
2.	Your application is in accordance with the design.



________________________________________



Task

1.	Register by mobile phone number;
2.	Place an order (hamburger+  ́ ́ ́+ Sumol de ananas);
3.	Receive the order in the restaurant;
4.	Make the delivery of the order to complete the process;
5.	Change payment method at the time of order delivery.



________________________________________



Design

The design must represent the appearance set in these Mockup.
Front-End
We prefer that you use React. Plus, use the front-end libraries you feel comfortable with.



________________________________________



Instructions for Sending The Test

1.	Please record your test code in your GitHub repository, and share the results with us.
2.	Remember that when you share, we recommend that you leave the folder as published and then send us an email, with the following features {nome_sobrenome}  LINK.
3.	Please don't forget to put the answers in the body of the email referring to the questions.

